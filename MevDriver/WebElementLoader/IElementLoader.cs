﻿using MevDriver.Data;
using OpenQA.Selenium;

namespace MevDriver.WebElementLoader
{
	public interface IElementLoader
	{
		IPageElement Element(By by);
	}
}
