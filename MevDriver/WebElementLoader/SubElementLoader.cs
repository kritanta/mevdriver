﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;

namespace MevDriver.WebElementLoader
{
	public class SubElementLoader
	{
		readonly IWebElement webElement;

		public SubElementLoader(IWebElement webElement)
		{
			this.webElement = webElement;
		}

		public IWebElement FindAndWaitForSubElement(By by)
		{
			Func<IWebElement> findMethod = () => this.webElement.FindElement(by);

			return SubElementLoader.FindAndWait(findMethod, 2);
		}

		public IEnumerable<IWebElement> FindAndWaitForSubElements(By by)
		{
			Func<IEnumerable<IWebElement>> findMethod = () => this.webElement.FindElements(by);

			return SubElementLoader.FindAndWait(findMethod, 2);
		}

		private static T FindAndWait<T>(Func<T> findMethod, int secondsToWait) where T : class
		{
			int searchIntervalMs = 100;
			int maxSearches = secondsToWait * 10;

			T result = null;

			for (int i = 1; i <= maxSearches && result == null; i++)
			{
				try
				{
					result = findMethod();
				}
				catch (NoSuchElementException)
				{
					Thread.Sleep(searchIntervalMs);
				}
			}

			return result;
		}
	}
}
