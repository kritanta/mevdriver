﻿using System;
using MevDriver.Data;
using OpenQA.Selenium;

namespace MevDriver.WebElementLoader
{
	public class StrictElementLoaderDecorator: IElementLoader
	{
		private readonly IElementLoader elementLoader;

		public StrictElementLoaderDecorator(IElementLoader elementLoader)
		{
			this.elementLoader = elementLoader;
		}

		public IPageElement Element(By by)
		{
			try
			{
				return this.elementLoader.Element(by);
			}
			catch (WebDriverTimeoutException e)
			{
				Console.WriteLine("Failed to find Element:", by.ToString());
				
				throw e;
			}
		}
	}
}
