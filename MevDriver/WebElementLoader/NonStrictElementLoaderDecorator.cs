﻿using System;
using MevDriver.Data;
using OpenQA.Selenium;

namespace MevDriver.WebElementLoader
{
	public class NonStrictElementLoaderDecorator: IElementLoader
	{
		private readonly IElementLoader elementLoader;

		public NonStrictElementLoaderDecorator(IElementLoader elementLoader)
		{
			this.elementLoader = elementLoader;
		}

		public IPageElement Element(By by)
		{
			try
			{
				return this.elementLoader.Element(by);
			}
			catch (WebDriverTimeoutException)
			{
				Console.WriteLine("Failed to find Element:", by.ToString());

				return new NullPageElement();
			}
		}
	}
}
