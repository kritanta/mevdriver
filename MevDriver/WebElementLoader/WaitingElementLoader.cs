﻿using MevDriver.Data;
using MevDriver.Extensions;
using OpenQA.Selenium;

namespace MevDriver.WebElementLoader
{
	public class WaitingElementLoader : IElementLoader
	{
		private readonly IWebDriver driver;
		private readonly int waitTimeSeconds;

		public WaitingElementLoader(IWebDriver driver, int waitTimeSeconds)
		{
			this.driver = driver;
			this.waitTimeSeconds = waitTimeSeconds;
		}

		public IPageElement Element(By by)
		{
			IWebElement webElement = this.driver.WaitForElement(by, this.waitTimeSeconds);

			return new PageElement(webElement, new SubElementLoader(webElement), this.driver);
		}
	}
}
