﻿using System.Collections.Generic;
using System.Drawing;
using OpenQA.Selenium;

namespace MevDriver.Data
{
	public class NullPageElement : IPageElement
	{
		public NullPageElement()
		{
		}

		public void Click()
		{
		}

        public void DoubleClick()
        {
        }

        public void ClickOffset(int x, int y)
        {
        }

        public void Clear()
		{
		}

		public bool IsPresentInDocument()
		{
			return false;
		}

		public bool IsVisible()
		{
			return false;
		}

		public bool BecomesVisible()
		{
			return false;
		}

		public bool IsEnabled()
		{
			return false;
		}

		public Point Location()
		{
			return new Point();
		}

		public void Write(string text)
		{
		}

        public void PressEnter()
        {
        }

        public bool Contains(string text)
		{
			return false;
		}

        public void SelectByDisplayText(string value)
        {
        }

        public string GetText()
		{
			return "";
		}

		public IPageElement GetFirstElement(By by)
		{
			return new NullPageElement();
		}

		public IEnumerable<IPageElement> ChildElements(By by)
		{
			return null;
		}

		public string GetAttribute(string attributeName)
		{
			return "";
		}

		public string GetStyle(string propertyName)
		{
			return "";
		}
	}
}
