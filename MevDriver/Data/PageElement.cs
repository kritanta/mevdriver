﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using MevDriver.Extensions;
using MevDriver.WebElementLoader;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace MevDriver.Data
{
	public class PageElement : IPageElement
	{
		private readonly IWebElement webElement;
		private readonly SubElementLoader subElementLoader;
        private readonly IWebDriver driver;

        public PageElement(IWebElement webElement, SubElementLoader subElementLoader, IWebDriver driver)
		{
			this.webElement = webElement;
			this.subElementLoader = subElementLoader;
            this.driver = driver;
        }

        public void RunExtension(Action<IWebDriver, IWebElement> action)
        {
            action(this.driver, this.webElement);
        }

        public T RunExtension<T>(Func<IWebDriver, IWebElement, T> action)
        {
            return action(this.driver, this.webElement);
        }

        public void GetElement(By by, Func<IWebDriver, bool> condition, int timeOutInSeconds = 5)
        {
            this.driver.WaitForElement(by, timeOutInSeconds, condition);
        }

        public void Click()
        {
            this.RepeatInteractionWrapper(() => this.webElement.Click());
        }

        private void RepeatInteractionWrapper(Action action)
        {
            for (int i = 0; i < 6; i++)
            {
                try
                {
                    action();
                    return;
                }
                catch (ElementClickInterceptedException)
                {
                    Thread.Sleep(500);
                }
                catch (ElementNotInteractableException)
                {
                    Thread.Sleep(500);
                }
            }
        }

        public void DoubleClick()
        {
            this.RepeatInteractionWrapper(() => new Actions(this.driver).DoubleClick(this.webElement).Build().Perform());
        }

        public void ClickOffset(int x, int y)
        {
            this.RepeatInteractionWrapper(() => new Actions(this.driver).MoveToElement(this.webElement).MoveByOffset(x, y).Click().Build().Perform());
        }

        public void Clear()
		{
            this.RepeatInteractionWrapper(() => this.webElement.Clear());
		}

		public bool IsPresentInDocument()
		{
			return true;
		}

		public bool IsVisible()
		{
			return this.webElement.Displayed;
		}

		public bool BecomesVisible()
		{
			for (int i = 0; i < 30; i++)
			{
				if (this.webElement.Displayed)
				{
					//allow for possible animation to run
					Thread.Sleep(500);

					return true;
				}
				else
				{
					Thread.Sleep(100);
				}
			}

			return false;
		}

		public bool IsEnabled()
		{
			return this.webElement.Enabled;
		}

		public Point Location()
		{
			return this.webElement.Location;
		}

		public void Write(string text)
		{
			this.webElement.SendKeys(text);
		}

        public void PressEnter()
        {
            this.Write(Keys.Enter);
        }

        public bool Contains(string text)
		{
			return this.webElement.ContainsText(text);
		}

        public void SelectByDisplayText(string value)
        {
            new SelectElement(this.webElement).SelectByText(value);
        }

        public string GetText()
		{
            if (!string.IsNullOrEmpty(this.webElement.Text))
            {
                return this.webElement.Text;
            }
            else if (!string.IsNullOrEmpty(this.webElement?.GetAttribute("value")))
            {
                return this.webElement?.GetAttribute("value");
            }

            return string.Empty;
        }

		public IPageElement GetFirstElement(By by)
		{
			IWebElement element = this.subElementLoader.FindAndWaitForSubElement(by);

			if (element == null)
			{
				return new NullPageElement();
			}

			return new PageElement(element, new SubElementLoader(this.webElement), this.driver);
		}

		public IEnumerable<IPageElement> ChildElements(By by)
		{
			IEnumerable<IWebElement> elements = this.subElementLoader.FindAndWaitForSubElements(by);

			if (elements == null)
			{
				return Enumerable.Empty<IPageElement>();
			}

			return this.webElement.FindElements(by).Select(i => new PageElement(i, new SubElementLoader(this.webElement), this.driver));
		}

		public string GetAttribute(string attributeName)
		{
			return this.webElement.GetAttribute(attributeName);
		}

		public string GetStyle(string propertyName)
		{
			return this.webElement.GetCssValue(propertyName);
		}
	}
}
