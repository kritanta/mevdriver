﻿using OpenQA.Selenium;

namespace MevDriver.Data
{
	public class ExpiringPageDecorator : IPage
	{
		private readonly IPage page;

		public ExpiringPageDecorator(IPage page)
		{
			this.page = page;
		}

		public IPageElement GetElement(By by)
		{
			this.CheckExpired();
			return this.page.GetElement(by);
		}

		public IPageElement GetBody()
		{
			this.CheckExpired();
			return this.page.GetBody();
		}

		public bool IsImage()
		{
			this.CheckExpired();
			return this.page.IsImage();
		}

        public void WaitForElementToBeHidden(By by, int timeOutInSeconds)
        {
            this.CheckExpired();
            this.page.WaitForElementToBeHidden(by, timeOutInSeconds);
        }

        public string GetRawTextResponse()
		{
			this.CheckExpired();
			return this.page.GetRawTextResponse();
		}

		private void CheckExpired()
		{
			//throw exception if expired
			// If the page has already been navigated away from it should be expired?
		}
	}
}
