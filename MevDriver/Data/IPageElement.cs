﻿using System.Collections.Generic;
using System.Drawing;
using OpenQA.Selenium;

namespace MevDriver.Data
{
	public interface IPageElement
	{
		IEnumerable<IPageElement> ChildElements(By by);
		void Click();
		void DoubleClick();
        void ClickOffset(int x, int y);
        void Clear();
		bool Contains(string text);
		string GetAttribute(string attributeName);
		string GetStyle(string propertyName);
		IPageElement GetFirstElement(By by);

		void SelectByDisplayText(string value);
		string GetText();
		bool IsPresentInDocument();
		bool IsVisible();
		bool BecomesVisible();
		bool IsEnabled();
		Point Location();
		void Write(string text);
        void PressEnter();
    }
}
