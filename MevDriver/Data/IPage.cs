﻿using OpenQA.Selenium;

namespace MevDriver.Data
{
	public interface IPage
	{
		IPageElement GetElement(By by);
		IPageElement GetBody();

		string GetRawTextResponse();
		bool IsImage();
        void WaitForElementToBeHidden(By by, int timeOutInSeconds);
    }
}
