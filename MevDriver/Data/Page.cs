﻿using System;
using MevDriver.Extensions;
using MevDriver.WebElementLoader;
using OpenQA.Selenium;

namespace MevDriver.Data
{
	public class Page : IPage
	{
		private readonly IWebDriver driver;
		private readonly IElementLoader elementLoader;

		public Page(IWebDriver driver, IElementLoader elementLoader)
		{
			this.driver = driver;
			this.elementLoader = elementLoader;
		}

		public IPageElement GetElement(By by)
		{
			return this.elementLoader.Element(by);
		}

        public void WaitForElementToBeHidden(By by, int timeOutInSeconds)
        {
            this.driver.WaitForElementToNotBeVisible(by, 5, timeOutInSeconds);
        }

        public void GetElement(By by, Func<IWebDriver, bool> condition, int timeOutInSeconds = 5)
        {
            this.driver.WaitForElement(by, timeOutInSeconds, condition);
        }

		public IPageElement GetBody()
		{
			return this.GetElement(By.TagName("body"));
		}

		public bool IsImage()
		{
			// Note: this relies on browsers displaying a "source" of an IMG tag

			string xPath = "//img[@src='" + this.driver.Url + "']";

			IPageElement image = this.GetElement(By.XPath(xPath));

			return image.IsVisible();
		}

		public string GetRawTextResponse()
		{
			// Note: this relies on browsers displaying a "source" of a PRE tag

			string xPath = "//pre";

			IPageElement pre = this.GetElement(By.XPath(xPath));

			return pre.GetText();
		}
	}
}
