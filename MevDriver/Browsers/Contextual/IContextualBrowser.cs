﻿using MevDriver.Data;

namespace MevDriver.Browsers.Contextual
{
	public interface IContextualBrowser: IBrowser
	{
		IPage CurrentPage();
	}
}
