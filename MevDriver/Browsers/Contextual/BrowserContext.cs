﻿using MevDriver.Data;

namespace MevDriver.Browsers.Contextual
{
	public class BrowserContext
	{
		public IPage CurrentPage { get; set; }
	}
}
