﻿using MevDriver.Data;

namespace MevDriver.Browsers.Contextual
{
	public class ContextualBrowser : IContextualBrowser
	{
		private readonly IBrowser browser;
		private IPage currentPage;

		public ContextualBrowser(IBrowser browser)
		{
			this.browser = browser;
		}

		public IPage LoadPage(string url)
		{
			return this.currentPage = this.browser.LoadPage(url);
		}

		public void Quit()
		{
			this.currentPage = null;
			this.browser.Quit();
		}

		public IPage CurrentPage()
		{
			return this.currentPage;
		}

		public IPage RefreshPage()
		{
			return this.currentPage = this.browser.RefreshPage();
		}
	}
}
