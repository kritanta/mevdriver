﻿using MevDriver.Data;
using MevDriver.PageLoader;
using OpenQA.Selenium;

namespace MevDriver.Browsers
{
	public class Browser : IBrowser
	{
		private readonly IPageLoader pageLoader;
		private readonly IWebDriver driver;

		public Browser(IWebDriver driver, IPageLoader pageLoader)
		{
			this.driver = driver;
			this.pageLoader = pageLoader;
		}

		public IPage LoadPage(string url)
		{
			return this.pageLoader.LoadPage(url);
		}

		public IPage RefreshPage()
		{
			return this.pageLoader.RefreshPage();
		}



		public void Quit()
		{
			this.driver.Close();
			this.driver.Quit();
            this.driver?.Dispose();
		}
	}
}
