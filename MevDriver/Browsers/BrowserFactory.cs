﻿using MevDriver.Browsers.Contextual;
using MevDriver.PageLoader;
using MevDriver.WebElementLoader;
using OpenQA.Selenium;

namespace MevDriver.Browsers
{
	public class BrowserFactory
	{
		private readonly IWebDriver driver;
		private readonly string relativeUrl;

		public BrowserFactory(IWebDriver driver, string relativeUrl)
		{
			this.driver = driver;
			this.relativeUrl = relativeUrl;
		}

		public IContextualBrowser CreateContextualDefaultBrowser()
		{
			IBrowser browser = this.CreateDefaultBrowser();

			return new ContextualBrowser(browser);
		}

		public IBrowser CreateDefaultBrowser()
		{
			IPageLoader pageLoader = this.CreatePageLoader();

			return new Browser(this.driver, pageLoader);
		}

		private IPageLoader CreatePageLoader()
		{
			IElementLoader elementLoader = this.CreateElementLoader();

			IPageLoader pageLoader = new RelativePageLoader(this.driver, elementLoader, this.relativeUrl);

			pageLoader = new ExpiringPageLoaderDecorator(pageLoader);

			return pageLoader;
		}

		private IElementLoader CreateElementLoader()
		{
			IElementLoader elementLoader = new WaitingElementLoader(this.driver, 10);

			elementLoader = new NonStrictElementLoaderDecorator(elementLoader);

			return elementLoader;
		}

	}
}
