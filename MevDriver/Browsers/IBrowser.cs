﻿using MevDriver.Data;

namespace MevDriver.Browsers
{
	public interface IBrowser
	{
		IPage LoadPage(string url);
		IPage RefreshPage();
		void Quit();
	}
}
