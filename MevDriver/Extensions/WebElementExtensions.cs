﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace MevDriver.Extensions
{
	public static class WebElementExtensions
	{
		public static bool ContainsText(this IWebElement element, string text)
		{
			return element.Text.Contains(text) || element?.GetAttribute("value")?.Contains(text) == true;
		}

        public static bool ContainsText(this IEnumerable<IWebElement> elements, string text)
		{
            return elements.Any(elem => elem.Text.Contains(text));
		}

        public static bool HasAttributeValue(this IWebElement element, string attribute, string value)
        {
            return String.Equals(element.GetAttribute(attribute), value);
        }

        public static void EnterText(this IWebElement element, By textBoxLocator, string textBoxValue)
		{
			element.FindElement(textBoxLocator).EnterText(textBoxValue);
		}

		public static void EnterText(this IWebElement element, string textBoxValue)
		{
			element.Click();
			element.Clear();
			element.SendKeys(textBoxValue);
		}

		public static decimal? GetNumericCssValue(this IWebElement element, string propertyName)
		{
			string value = element.GetCssValue(propertyName);

			if (value == null)
			{
				return null;
			}

			string dimensionlessValue = WebElementExtensions.TrimUnits(value);

			if (Decimal.TryParse(dimensionlessValue, out decimal numericValue))
			{
				return numericValue;
			}

			return null;
		}

		private static string TrimUnits(string input)
		{
			string[] units = { "px", "em", "pt" };

			foreach (string unit in units)
			{
				input = input.Replace(unit, String.Empty);
			}

			return input;
		}
	}
}
