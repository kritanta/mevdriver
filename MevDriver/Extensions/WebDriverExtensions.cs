﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MevDriver.Extensions
{
	public static class WebDriverExtensions
	{
		public static void Wait(this IWebDriver driver, int milliseconds = 1000)
        {
            Thread.Sleep(milliseconds);
        }

		public static IWebElement WaitForElement(this IWebDriver driver, By by, int timeoutInSeconds = 10)
		{
			if (timeoutInSeconds > 0)
			{
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

				return wait.Until(drv => drv.FindElement(by));
			}

			return driver.FindElement(by);
		}

        public static bool WaitForCondition(this IWebDriver driver, int timeoutInSeconds, Func<IWebDriver, bool> action)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

            return wait.Until(action);
        }

        public static IWebElement WaitForElement(this IWebDriver driver, By by, int timeoutInSeconds, Func<IWebDriver, bool> action)
        {
            if (driver.WaitForCondition(timeoutInSeconds, action))
            {
                return driver.WaitForElement(by, timeoutInSeconds);
            }

            throw new Exception("Condition not met");
        }

        public static void WaitForElementToNotBeVisible(this IWebDriver driver, By by, int initialWaitInSeconds = 5, int loadWaitInSeconds = 20)
        {
            bool elementLocatedAndVisible = WebDriverExtensions.ElementLocatedAndVisible(driver, @by, initialWaitInSeconds);

            if (elementLocatedAndVisible)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(loadWaitInSeconds));

                try
                {
                    // Wait until either its not found or its found and not visible
                    wait.Until(drv => (drv.FindElement(@by)?.Displayed != true));
                }
                catch (WebDriverTimeoutException)
                {
                    //element does not appear within initial window so assume its not present.
                }
            }
        }

        private static bool ElementLocatedAndVisible(IWebDriver driver, By by, int initialWaitInSeconds)
        {
            try
            {
                var elem = driver.WaitForElement(by, initialWaitInSeconds);
                return elem.Displayed;
            }
            catch (WebDriverTimeoutException)
            {
                //element does not appear within initial window so assume its not present.
            }

            return false;
        }

        public static ReadOnlyCollection<IWebElement> WaitForElements(this IWebDriver driver, By by, int timeoutInSeconds = 10)
		{
			if (timeoutInSeconds > 0)
			{
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

				return wait.Until(drv => (drv.FindElements(by).Count > 0) ? drv.FindElements(by) : null);
			}

			return driver.FindElements(by);
		}

		public static ReadOnlyCollection<IWebElement> WaitForElementsByTag(this IWebDriver driver, string tagName, int timoutInSeconds = 10)
		{
			return driver.WaitForElements(By.TagName(tagName), timoutInSeconds);
		}

		public static IWebElement WaitForFormElementByLabel(this IWebDriver driver, string labelText, int timoutInSeconds = 10)
		{
			ReadOnlyCollection<IWebElement> labels = driver.WaitForElementsByTag("label", timoutInSeconds);

			IWebElement label = labels.FirstOrDefault(l => l.Text.Equals(labelText));

			return driver.WaitForElement(By.Id(label.GetAttribute("for")), timoutInSeconds);
		}


		public static void WaitForPopup(this IWebDriver driver, string pageUrl)
		{
			WebDriverExtensions.WaitForPopup(driver, pageUrl, 60);
		}

		public static void WaitForPopup(this IWebDriver driver, string pageUrl, int timeoutSeconds)
		{
            while (!driver.Url.StartsWith(pageUrl) && timeoutSeconds > 0)
			{
				bool allHandlesAreUrls = true;

				foreach (string handle in driver.WindowHandles)
				{
					allHandlesAreUrls = driver.Url.StartsWith("http");

					driver.SwitchTo().Window(handle);

					if (driver.Url.StartsWith(pageUrl))
					{
						break;
					}
				}

				if (allHandlesAreUrls)
				{
					break;
				}

				Thread.Sleep(1000);
                timeoutSeconds--;
			}
		}

        public static void EnterText(this IWebDriver driver, By textBoxLocator, string textBoxValue, int timeoutSeconds = 10)
		{
            driver.WaitForElement(textBoxLocator, timeoutSeconds).EnterText(textBoxValue);
		}
	}
}
