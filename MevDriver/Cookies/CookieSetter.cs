﻿using System;
using MevDriver.Extensions;
using OpenQA.Selenium;

namespace MevDriver.Cookies
{
	public class CookieSetter
	{
		private readonly IWebDriver driver;
		private readonly string cookieSettingUrl;

		public CookieSetter(IWebDriver driver, string cookieSettingUrl)
		{
			this.driver = driver;
			this.cookieSettingUrl = cookieSettingUrl;
		}

		public void SetCookie(string cookieName, string cookieValue)
		{
			this.EnsurePageLoadedSoCookiesCanBeSet();

			Cookie cookie = new Cookie(cookieName, cookieValue);

			this.driver.Manage().Cookies.AddCookie(cookie);

			Console.WriteLine(@"Finished setting cookie ""{0}"" to ""{1}""", cookieName, cookieValue);
		}

		private void EnsurePageLoadedSoCookiesCanBeSet()
		{
			this.driver.Navigate().GoToUrl(this.cookieSettingUrl);

			this.driver.WaitForElement(By.TagName("body"));
		}
	}
}
