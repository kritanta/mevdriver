﻿using OpenQA.Selenium;

namespace MevDriver.Cookies
{
	public class CookieManagerFactory
	{
		public CookieManager CreateDefaultCookieManager(IWebDriver driver, string cookieSettingUrl)
		{
			CookieSetter cookieSetter = new CookieSetter(driver, cookieSettingUrl);

			return new CookieManager(cookieSetter);
		}
	}
}
