﻿namespace MevDriver.Cookies
{
	public class CookieManager
	{
		private CookieSetter _cookieSetter;
		public CookieManager(CookieSetter cookieSetter)
		{
			this._cookieSetter = cookieSetter;
		}

		public void SetCookie(string cookieName, string cookieValue)
		{
			this._cookieSetter.SetCookie(cookieName, cookieValue);
		}
	}
}
