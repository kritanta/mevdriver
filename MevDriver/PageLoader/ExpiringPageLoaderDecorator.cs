﻿using MevDriver.Data;

namespace MevDriver.PageLoader
{
    public class ExpiringPageLoaderDecorator : IPageLoader
    {
		private readonly IPageLoader pageLoader;

		public ExpiringPageLoaderDecorator(IPageLoader pageLoader)
		{
			this.pageLoader = pageLoader;
		}

		public IPage LoadPage(string url)
		{
			return new ExpiringPageDecorator(this.pageLoader.LoadPage(url));
		}

		public IPage RefreshPage()
		{
			return new ExpiringPageDecorator(this.pageLoader.RefreshPage());
		}
    }
}
