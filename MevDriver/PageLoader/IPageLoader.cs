﻿using MevDriver.Data;

namespace MevDriver.PageLoader
{
	public interface IPageLoader
	{
		IPage LoadPage(string relativeUrl);
		IPage RefreshPage();
	}
}
