﻿using System;
using MevDriver.Data;
using MevDriver.WebElementLoader;
using OpenQA.Selenium;

namespace MevDriver.PageLoader
{
    public class RelativePageLoader : IPageLoader
    {
		private readonly IWebDriver driver;
		private readonly string baseUrl;
		private readonly IElementLoader elementLoader;

		public RelativePageLoader(IWebDriver driver, IElementLoader elementLoader, string baseUrl)
		{
			this.driver = driver;
			this.baseUrl = baseUrl;
			this.elementLoader = elementLoader;
		}

		public IPage LoadPage(string relativeUrl)
		{
			string newUrl = string.Concat(this.baseUrl.TrimEnd('/'), "/", relativeUrl.TrimStart('/'));

			Console.WriteLine("Loading page: " + newUrl);

			this.driver.Navigate().GoToUrl(newUrl);

			return new Page(this.driver, this.elementLoader);
		}

		public IPage RefreshPage()
		{
			Console.WriteLine("Refreshing page");

			this.driver.Navigate().Refresh();

			return new Page(this.driver, this.elementLoader);
		}
    }
}
